# Arak Linux Users Group official website

If you want to contribute in Arak LUG website you have to:

- Fork this project
- Commit to the project and push your commits to your fork
- Send a merge request to the [maintainer](https://gitlab.com/sinaaghighi)

# Social Media accounts

- [Instagram](https://instagram.com/arak_lug)
- [Twitter](https://twitter.com/arak_lug)
- [Telegram](https://t.me/arak_lug)
- [Mastodon](https://mastodon.social/arak_lug)
